#!/usr/bin/env bash
set -e
# Dotfiles' project root directory
ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
# Host file location
HOSTS="$ROOTDIR/src/hosts"

# Main playbook
PLAYBOOK="$ROOTDIR/src/ergod.yml"


# Runs Ansible playbook using our user.

# Run the prerun tag to setup Ansible
ansible-playbook -i "$HOSTS" "$PLAYBOOK" --ask-become-pass --tags="prerun"

# Setup the target computer
ansible-playbook -i "$HOSTS" "$PLAYBOOK" --ask-become-pass --skip-tags="prerun"
exit 0

