# Ergod - Sway DE

Ansible playbook to install and configure a Sway Desktop Environment on Arch based systems.

Essentially, does all the heavy lifting to deploy [arran/dotfiles](https://codeberg.org/arran/dotfiles).


## Install

First, install `ansible`.

Then, deploy! 🤘

```shell
./deploy.sh
```

## Customize

Chezmoi is leveraged to manage the configuration (dotfiles) - Templating to used to provide customization. 

Once deployed, edit the configuration by running the following.

```shell
chezmoi edit-config
```
